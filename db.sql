create database supermercado;

CREATE TABLE sucursales (
    id int not null,
    calle text,
	ciudad text,
	cp text,
	provincia text,
	nombre text not null,
	primary key (id)
);

create table producto(
id int not null,
codigo bigint not null,
descripcion varchar(250) not null,
cantidad int not null default 0,
primary key(id)
);

create table precio_producto(
id_producto int not null,
fecha datetime not null,
precio int not null,
primary key(id_producto,fecha),
foreign key(id_producto)
references producto(id)
);
